from hashlib import md5,sha1
import binascii
import sys,os
def isFileExist(path):
    if not os.path.exists(path):
        path=os.path.join(os.path.dirname(__file__),path)
        if not os.path.exists(path):
            return False
    return True

def getCRC32(path):
    blocksize=1024*10
    f=open(path,'rb')
    buff=f.read(blocksize)
    crc=0
    while(len(buff)!=0):
        crc=binascii.crc32(buff,crc)
        buff=f.read(blocksize)
    f.close()
    return 'CRC32:'+str(hex(crc&0xffffffff))[2:10]

def getHashByString(string):
    md5_str=md5()
    sha1_str=sha1()
    md5_str.update(string)
    sha1_str.update(string)
    crc=0
    crc=binascii.crc32(string,crc)
    crc_str=str(hex(crc&0xffffffff))[2:10]
    print 'Get HashByString Mode:"'+string+'"\n'
    print 'CRC32:'+crc_str
    print 'md5:'+md5_str.hexdigest()
    print 'sha1:'+md5_str.hexdigest()

def getHashByFile(hashtype,path):
    if not isFileExist(path):
        print("Error:Path not exist.")
        getHashByString(path)
        
    if (hashtype=='md5'):
        m=md5()
    elif (hashtype=='sha1'):
        m=sha1()
    elif (hashtype=='crc32'):
        return getCRC32(path)
    else:
        return 

    f=open(path,'rb')
    b=f.read(10240)
    while(b!=''):
        m.update(b)
        b=f.read(10240)
    f.close()
    return hashtype+':'+m.hexdigest()

if __name__=='__main__':
    if len(sys.argv)>2:
        hashtype=sys.argv[1]
        for fname in sys.argv[2:]:
            res=getHashByFile(hashtype,fname)
            print res
    if len(sys.argv)==2:
        fname=sys.argv[1]
        if not isFileExist(fname):
            getHashByString(fname)
        else:
            print getHashByFile('crc32',fname)
            print getHashByFile('md5',fname)
            print getHashByFile('sha1',fname)
    else:
        print("GetHash.py [type] [path]")

    a=raw_input("Press Enter to exit.")
    exit(0)
